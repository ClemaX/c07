/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_split.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 20:02:48 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 23:01:41 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	const char *start = str;

	while (*str)
		str++;
	return (str - start);
}

char	*ft_strdup(char *src)
{
	char	*dup;
	char	*start;

	dup = malloc(sizeof(char) * (ft_strlen(src) + 1));
	if (!dup)
		return (NULL);
	start = dup;
	while (*src)
	{
		*dup = *src;
		src++;
		dup++;
	}
	*dup = '\0';
	return (start);
}

int		in_str(char *str, char c)
{
	while (*str)
	{
		if (c == *str)
			return (1);
		str++;
	}
	return (0);
}

int		replace(char *str, char *charset, char substitute)
{
	int i;
	int words;

	i = 0;
	words = 1;
	while (str[i])
	{
		if (in_str(charset, str[i]))
			str[i] = substitute;
		if (i != 0 && str[i] != substitute)
			words++;
		i++;
	}
	return (words);
}

char	**ft_split(char *str, char *charset)
{
	char	*dest;
	char	**list;
	char	**start;
	int		i;

	if (!(dest = ft_strdup(str)))
		return (NULL);
	if (!(list = malloc(sizeof(char*) * (replace(dest, charset, '\0') + 1))))
		return (NULL);
	start = list;
	i = 0;
	while (i <= ft_strlen(str))
	{
		if (dest[i] == '\0')
			i++;
		else
		{
			*list = &dest[i];
			list++;
			while (dest[i] != '\0')
				i++;
		}
	}
	*list = 0;
	return (start);
}
