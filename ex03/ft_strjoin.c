/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strjoin.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 20:19:25 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 22:46:00 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_strslen(int size, char **strs, char *sep)
{
	char	*str;
	int		len_sep;
	int		len;
	int		i;

	i = 0;
	len = 0;
	len_sep = 0;
	while (sep[len_sep])
		len_sep++;
	while (i < size)
	{
		str = *strs;
		while (*str)
		{
			str++;
			len++;
		}
		if (i < size - 1)
			len += len_sep;
		strs++;
		i++;
	}
	return (len);
}

char	*ft_strcat(char *dest, char *src)
{
	char *start;

	start = dest;
	while (*dest != '\0')
		dest++;
	while (*src != '\0')
	{
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
	return (start);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*str;
	int		strlen;
	int		i;

	if (size == 0)
	{
		str = malloc(sizeof(char));
		*str = '\0';
		return (str);
	}
	strlen = ft_strslen(size, strs, sep);
	str = malloc(sizeof(char) * strlen + 1);
	if (!str)
		return (NULL);
	*str = 0;
	i = 0;
	while (i < size)
	{
		ft_strcat(str, *strs);
		if (i < size - 1)
			ft_strcat(str, sep);
		strs++;
		i++;
	}
	return (str);
}
