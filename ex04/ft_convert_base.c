/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_convert_base.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/07 20:59:14 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 17:09:02 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

int				ft_atoi_base(char *str, char *base);
unsigned int	is_base_valid(char *base);
unsigned int	ft_strlen(char *str);

void			next_char(char *str, unsigned int nb, char *base, int base_n)
{
	char r;

	if (nb)
	{
		r = base[nb % base_n];
		next_char(str - 1, nb / base_n, base, base_n);
		*str = r;
	}
}

int				count_digits(unsigned int value, int base_n)
{
	int count;

	count = 0;
	while (value /= base_n)
		count++;
	return (count);
}

char			*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	const int			base_to_len = ft_strlen(base_to);
	char				*str;
	unsigned int		str_size;
	int					value;
	unsigned int		u_nb;

	if (!is_base_valid(base_from)
	|| !is_base_valid(base_to))
		return (NULL);
	value = ft_atoi_base(nbr, base_from);
	u_nb = value < 0 ? value * -1 : value;
	str_size = value < 0 ? 2 : 1;
	str_size += count_digits(u_nb, base_to_len);
	str = malloc(sizeof(char) * str_size);
	if (!str)
		return (NULL);
	if (value < 0)
		str[0] = '-';
	next_char(str + str_size - 1, u_nb, base_to, base_to_len);
	str[str_size] = '\0';
	return (str);
}
