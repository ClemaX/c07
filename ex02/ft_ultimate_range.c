/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ultimate_range.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 18:02:56 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/13 21:31:19 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	const int	size = max - min;
	int			i;

	if (size <= 0)
		return (0);
	*range = malloc(sizeof(int) * size);
	if (!range)
		return (0);
	i = 0;
	while (min < max)
		*(*range + i++) = min++;
	return (size);
}
